const express = require('express');
const app = express()
const port = 8080
const requestIp = require('request-ip');
const bodyParser = require('body-parser')
const expressIp = require('express-ip')

app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(bodyParser.raw())
app.use(expressIp().getIpInfoMiddleware)



app.post("/app", (req, res) => {
    var ip = req.connection.remoteAddress
    var data = req.body.payload
    console.log(`Ip : ${ip}`);
    console.log(`data: ${JSON.stringify(data)}`);


    res.end()
})

app.listen(port)
console.log(`APP Running on Port: ${port}`);